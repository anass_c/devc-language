%{
/**
Copyright 2020 Anass Chaaraoui

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include<stdio.h>
#include<stdlib.h>
#include "tools.h"

extern FILE* yyin;
extern void yyerror();
extern int yylex();
extern int yylineno;

char* buffer;

%}

//keywords
%token PRINTLN IF IFNOT OR TRUE FALSE
// math
%token PLUS MINUS DIV MULT EQUALS
// symboles
%token SEMICOLON COMMA BIG_THAN LESS_THAN OPEN_PARENTH CLOSE_PARENTH

%union{
    int integer;
    char* type;
    char* string;
    char* id;
}

%token <integer> INTEGER
%token <type> TYPE
%token <string> STRING
%token <integer> ID

%type <integer> term
%type <integer> expression
%type <integer> variable


%left PLUS MINUS    // less priority
%left MULT DIV      // high priority

%%

statements  : statement {printf("SUCCEED\n"); YYACCEPT;}
            ;

statement   : TYPE list_of_variables SEMICOLON 
            | statement TYPE list_of_variables SEMICOLON
            | PRINTLN OPEN_PARENTH expression CLOSE_PARENTH SEMICOLON           {printf("%s\n", itoa($3));}
            | statement PRINTLN OPEN_PARENTH expression CLOSE_PARENTH SEMICOLON {printf("%s\n", itoa($4));}
            ;

list_of_variables   : variable
                    | list_of_variables COMMA variable
                    ;

variable    : ID                                            {if(!isDuplicated(yylval.id)){saveID(yylval.id, 0);}
                                                            else idDuplicatedException(yylval.id);
                                                            }
            | ID  {buffer = yylval.id;} EQUALS expression   {if(!isDuplicated(buffer)){saveID(buffer, $4);}
                                                            else idDuplicatedException(buffer);
                                                            }
            ;


expression  :   OPEN_PARENTH expression CLOSE_PARENTH   {$$=$2;}
            |   expression MULT expression              {$$=$1*$3;}     
            |   expression DIV expression               {
                                                            if(!$3){yyerror("Divide by 0!!"); YYABORT;}
                                                            else $$=$1/$3;
                                                        }   
            |   expression PLUS expression              {$$=$1+$3;}    
            |   expression MINUS expression             {$$=$1-$3;}      
            |   MINUS expression                        {$$=-$2;}  
            |   PLUS expression                         {$$=$2;}  
            |   term                                    {$$=$1;}  
            ;

// read from symbole table
term        :   ID          {if(isDuplicated(yylval.id)){$$ = getValue(yylval.id);}
                            else variableNotDeclaredException(yylval.id);
                            }   
            |   INTEGER     {$$ = yylval.integer;}
            ;

%%

int main(){
    yyparse();
    printf("lines: %d\n", yylineno);
}
