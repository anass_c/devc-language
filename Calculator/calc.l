%{
/**
Copyright 2020 Anass Chaaraoui

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include<stdio.h>
#include<string.h>
#include "calc.tab.h"

void InvalidToken();
void extern yyerror(char*);
int col = 0;
%}

%option noyywrap

number  [0-9]+

%%

{number}    {col += strlen(yytext); yylval = atoi(yytext); return (NUMBER);}

"+"         {++col; return(PLUS);}
"-"         {++col; return(MINUS);}
"/"         {++col; return(DIV);}
"*"         {++col; return(MUL);}
"("         {++col; return(OPEN_PARTS);}
")"         {++col; return(CLOSE_PARTS);}

[\n]        {return yytext[0];}

[ ]         {++col;}

.           {++col; InvalidToken();}

%%

void yyerror(char* s){
    int i = 0;
    for(i = 0; i < col; i++){
        fprintf(stderr,"-");
    }
    fprintf(stderr,"^");
    fprintf(stderr, "\nError on column %d : %s\n", col, s);
    exit(0);
}

void InvalidToken(){
    int i = 1;
    for(i = 1; i < col; i++){
        printf("-");
    }
    printf("^");
    printf("\nError on column %d : Invalid Token %s\n", col, yytext);
    exit(0);
}

// int main(){

//     yylex();
//     return 0;
// }
