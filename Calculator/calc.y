%{
/**
Copyright 2020 Anass Chaaraoui

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include<stdio.h>

extern void yyerror();
extern int yylex();

int i = 0;
%}

%token NUMBER PLUS MINUS MUL DIV OPEN_PARTS CLOSE_PARTS


%left PLUS MINUS
%left MUL DIV

%%

line    :   expr '\n'    {printf("= %d\n", $$); YYACCEPT;}
        ;

expr    :    OPEN_PARTS expr CLOSE_PARTS     {$$ = $2;}
        |   expr OPEN_PARTS expr CLOSE_PARTS {$$ = $1 * $3;}
        |   expr MUL expr                    {$$ = $1 * $3;}
        |   expr DIV expr                   {
                                                if(!$3){yyerror("Divide by 0!!"); YYABORT;}
                                                else $$=$1/$3;
                                            }
        |   expr PLUS expr                  {$$ = $1 + $3;}
        |   expr MINUS expr                 {$$ = $1 - $3;}
        |   MINUS expr                      {$$ = -$2;}
        |   PLUS expr                       {$$ =  $2;}
        |   NUMBER                          {$$ = yylval;}
        ;

%%

int main(){
    yyparse();
    return 0;
}
