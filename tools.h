#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int nbrOfIdentifier = 0;
extern int yylineno;

struct symboleTable
{
    /* data */
    int value;
    char *id;
    char *dataType;
} symboleTable[30];

void saveID(char *id, int value)
{
    symboleTable[nbrOfIdentifier].id = id;
    symboleTable[nbrOfIdentifier].value = value;
    nbrOfIdentifier++;
}

bool isDuplicated(char *id)
{
    int i;
    for (i = 0; i < nbrOfIdentifier; i++)
    {
        if (strcmp(id, symboleTable[i].id) == 0)
        {
            return true;
        }
    }
    return false;
}

void idDuplicatedException(char *id)
{
    printf("ERROR ON LINE %d : \nDuplicate identifier '%s' found.\n", yylineno, id);
    exit(0);
}

void clearBuffers(char* buffer){
    int i=0;
    while(buffer[i] != '\0'){
        buffer[i] = '\0';
        i++;
    }
}

int getValue(char* id){
    int i;
    for(i=0;i<nbrOfIdentifier;i++){
        if(strcmp(id,symboleTable[i].id) == 0){
            return symboleTable[i].value;
        }
    }
    return 0;
}

void variableNotDeclaredException(char *id)
{
    printf("ERROR ON LINE %d : \nundeclared variable '%s' found.\n", yylineno, id);
    exit(0);
}

char* itoa(int number){
   static char buffer[33];
  snprintf(buffer, sizeof(buffer), "%d", number);
  return buffer;
}