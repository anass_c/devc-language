# This an attempt to create a programming language using flex & bison

To try **DevC** language install flex and bison and gcc on your machine first:

Perform the steps below to install the GCC Compiler on linux:

1. Install the ```build-essential``` package by typing:

        > sudo apt install build-essential

2. The command installs a bunch of new packages including gcc, g++ and make.
You may also want to install the manual pages about using GNU/Linux for development:

        > sudo apt-get install manpages-dev

3. To validate that the GCC compiler is successfully installed, use the gcc --version command which prints the GCC version:

        > gcc --version


To install ```flex``` and ```bison```:

        > sudo apt-get install flex
        > sudo apt-get install bison



## How to compile DevC programm:

1. write a program on a text file and name it using the suffix *.devc* or u can just use the sample.devc.

2. run the follwing commands to be able to compile ur program:

        lex devc.l
        bison -d devc.y
        gcc -o devc lex.yy.c devc.tab.c
        ./devc < sample.devc
    
tataaah it's working right !! let me know if not :)

You'll find attached documents that i have read to be able to create a programming language like me and why not contribute to this repo I would be more than happy.