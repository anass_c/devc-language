%{
/**
Copyright 2020 Anass Chaaraoui

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include<stdio.h>
#include"devc.tab.h"

void InvalidToken(char*);

%}

%option noyywrap

multiLineComment    [/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]

oneLineComment      "//".*

types               "int"

id                  [_a-zA-Z][_a-zA-Z0-9]*

integer             [0-9]+

string              \"(\\.|[^"])*\"

%%

{multiLineComment}  {}
{oneLineComment}    {}
{types}             {yylval.type = strdup(yytext); return TYPE;}

"println"           {return PRINTLN;}
"if"                {return IF;}
"ifnot"             {return IFNOT;}
"or"                {return OR;}
"true"              {return TRUE;}
"false"              {return FALSE;}

{id}                {yylval.id = strdup(yytext); return ID;}

{integer}           {yylval.integer = atoi(strdup(yytext)); return INTEGER;}

{string}            {yylval.string = strdup(yytext); return STRING;}

"="                 {return EQUALS;}
"-"                 {return MINUS;}
"+"                 {return PLUS;}
"*"                 {return MULT;}
"/"                 {return DIV;}
";"                 {return SEMICOLON;}
","                 {return COMMA;}
"("                 {return OPEN_PARENTH;}
")"                 {return CLOSE_PARENTH;}
"<"                 {return BIG_THAN;}
">"                 {return LESS_THAN;}

[ \t]+              {}

[\n]                {yylineno++;}

.                   {InvalidToken(yytext);}

%%
void InvalidToken(char* s){
    printf("Error on line %d : Invalid Token %s\n", yylineno, s);
    exit(0);
}

void yyerror(char* s){
    fprintf(stderr, "Error on line %d : %s\n", yylineno, s);
    exit(0);
}

// int main(){

//     yyin = fopen("sample.devc", "r");

//     yylex();
// }
